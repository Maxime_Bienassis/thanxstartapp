# Hello

Welcome to the Thanxngo recruitment exercice!

The goal of this exercice is to create a very simple mobile
application (iOS or Android) that scan a QR Code and display the
profile associated with the QR Code.

## The server

We provide a very simple webserver that exposes a rest API for profiles.
The server is available at: https://agile-beach-76323.herokuapp.com/

The api is under the `/api/` url:

- https://agile-beach-76323.herokuapp.com/api/people/ : list of people
- https://agile-beach-76323.herokuapp.com/api/people/?search=April : filter by names
- https://agile-beach-76323.herokuapp.com/api/people/?code=c242ebb1 : filter by code

## The app

The application must be writen witht React Native, using this provided
template.

You can use the https://expo.io app to run it.

There is 2 feature we want for the application:

### Search Feature

We want to add a text input where users can search people by their
names.

- First step is to use a submit button and display people returned by the API.
- If you have time try to add a search-as-you-type feature.

### QRcode scanning

We want to be able to scan a QRCode and directly display the user
profile.  It should display a button to start scanning a QR Code, if
the QR Code matches a code available on the server the app should
display the person name.

The QRCode you have to scan can be found here:
https://agile-beach-76323.herokuapp.com/ .
They only contain the encoded "code" value.

You should use an external library to scan the QRCode:
- If you use expo: https://docs.expo.io/versions/latest/sdk/bar-code-scanner/;
- If you use
  https://github.com/react-native-community/react-native-camera look
  for `barCodeTypes` and `onBarCodeRead`;

- If you have time handle the errors cases where the QRCode does not
  correspond to a profile.

## Development

The easiest way to get started is by using the embeded
[expo.io](https://expo.io/) tool.

Expo will allow you to directly see the result in your own device.
You have to install the expo app in your device.

To start use yarn in the directory to install all dependencies.

```
$ cd thanxstartapp
$ yarn
```

To run the expo tool just run `yarn start`
```
$ yarn start
```

Then scan the expo qrcode or send the link with sms.
